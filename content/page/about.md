---
title: About me
comments: false
---

Bonjour!

Je m'appelle Lætitia Avrot. Je suis experte PostgreSQL. Je suis assez investie
dans la communauté PostgreSQL:

- Je suis fondatrice de Postgres Women
- Je suis trésorière de postgreSQL Europe
- J'ai écris quelques patches
- J'ai organisé des événements
- J'ai été membre du comité du Code de Conduite de la communauté pendant 2 ans

Je suis ici pour vous faire découvrir le métier de DBA (cours magistral) et
PostgreSQL (TP).

À l'issue de cette formation, vous serez capables de :

- Installer Postgres sous Linux (manuellement et automatiquement avec ansible)
- Connaître les bases de l'optimisation
- Sauvegarder et Restaurer un cluster Postgres
- mettre en place une streaming replication

