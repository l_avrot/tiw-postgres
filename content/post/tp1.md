---
title: "TP1: Installation"
date: 2020-10-19T15:40:48+02:00
tag: "tp1"
---

## But de ce TP
Le but de ce TP est d'installer PostgreSQL sur vos VM Linux. Vous avez accès
à 2 VM. La première va vous permettre d'installer Postgres manuellement pour
bien comprendre les étapes à effectuer.
La deuxième devrait vous permettre d'installer automatiquement une nouvelle
instance de Postgres grâce à ansible.

La version installée sera la dernière version disponible.

## Travail demandé
Nous vous demandons de fournir en fin de TP un "runbook" de vos manipulations.

>Dans un système informatique ou un réseau, un "runbook", retranscrit
>littéralement dossier "d'exploitation", est une compilation systématique des
>procédures et des opérations que l'administrateur ou l'opérateur du système
>effectue.

Voir [Wikipedia](https://fr.wikipedia.org/wiki/Runbook)

Voici un exemple de runbook: [How to deploy WordPress with highly available
PostgreSQL](https://www.enterprisedb.com/postgres-tutorials/how-deploy-wordpress-highly-available-postgresql)

## A propos des sources
Nous vous demandons de faire très attention aux sources de vos différentes recherches
internet, tant n'importe qui peut dire n'importe quoi sur internet.

Voici quelques sources sécurisées:

- [Le site officiel de PostgreSQL](https://www.postgresql.org/)
- [La documentation officielle de
    PostgreSQL](https://www.postgresql.org/docs/current/index.html)
- [Le site 2nd Quadrant](https://www.2ndquadrant.com/en/)
- [Le site d'EDB](https://www.enterprisedb.com/)
- [Le site de Crunchy Data](https://www.crunchydata.com/)

2nd Quadrant, EDB and Crunchy Data sont les 3 sociétés qui embauchent le plus de
développeurs et experts de PostgreSQL.

Voici quelques examples de sources écrites par des personnes ne comprenant pas
Postgres:
- [Developpez.com](https://postgresql.developpez.com/articles/installer-postgresql/#LC) (Installation uniquement graphique, donc pas une installation officielle de PostgreSQL)
- [Comment ça
    marche](https://www.commentcamarche.net/contents/813-installation-de-postgresql)
    (Compilation des sources: chose à ne JAMAIS faire en production)
- [Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04)
    (Installation à partir des dépôts de la distribution au lieu de prendre les
    dépôts communautaires)

## Installation manuelle
Votre runbook devra détailler les différentes étapes (et le pourquoi de chaque
étape) de l'installation de PostgreSQL et de la création de l'instance. À la fin
de l'installation, vous configurerez Postgres pour qu'il puisse utiliser un
maximum d'un quart de la RAM de la VM comme mémoire partagée. 
Pour des raisons de sécurité, vous veillerez à ce que le chiffrement des mots de
passe se fasse en scram-sha-256.

En fin d'installation, vous vous connecterez à l'instance et créerez un
utilisateur "myuser" qui sera propriétaire d'une nouvelle base de données
"mydb".

Vous configurerez alors votre instance pour qu'elle accepte les connections
depuis la VM d'un autre binôme, sans ouvrir pour autant votre instance à toutes
les machines du monde. Ce même binôme aura lui aussi configuré son instance pour
accepter une connexion depuis votre VM. Vous vous connecterez alors à la base de
données mydb de l'autre binôme en utilisant le user myuser et le mot de passe
fourni par l'autre binôme.

## Installation automatique
Dans cette partie, le but est d'automatiser toutes les étapes de l'installation
manuelle en fournissant un runbook commenté qui fera référence à ces étapes.

De la même manière que pour l'installation manuelle, votre playbook devra
comporter la configuration de la mémoire partagéei, la configuration du mode de 
chiffrement des mots de passe ainsi que la création d'un utilisateur "myuser" et
d'une base de données "mydb", détenue par "myuser".

Enfin, votre playbook devra permettre l'accès à la base de données mydb avec
l'utilisateur myuser depuis la VM d'un autre binôme.

Vous testerez ensuite manuellement l'accès a la base de donnees de ce binôme
avec le mot de passe fourni.

## Allons encore un peu plus loin
Sur une des bases de données créées, vous créerez une table `mytable(myid integer, mydate
tmestamp with timezone)`, avec `myid` la clé primaire de `mytable`. Vous insérerez alors dans cette table un ensemble de nouvelles
lignes ayant comme valeur de `mydate` chaque minute entre le 27 novembre et
le 9 décembre de cette année.

## Vous êtes perdu ?
Si vous ne savez pas par quoi commencer, voici des questions pour vous
aiguiller:

1. Quelle est la dernière version de PostgreSQL ?
2. Quel site vous paraît le plus approprié pour trouver la procédure
   d'installation de PostgreSQL ?
3. Allez sur ce site dites-vous bien qu'il doit y avoir un gros bouton
   pointant sur la section que vous cherchez...
4. Regardez bien les étapes décrites et copiez-collez les commandes
5. Si vous avez des soucis de connexion à une instance, suivez ce guide: [Can't
   connect to Postgres](https://mydbanotebook.org/post/cant-connect/).
5. Pour ansible, sur quel site pensez-vous trouver des informations ?
6. Pour un bon démarrage d'ansible du point de vue de Postgres, vous pouvez lire
   le post de blog de ma copine Gülçin: [https://www.2ndquadrant.com/en/blog/ansible-loves-postgresql/](https://www.2ndquadrant.com/en/blog/ansible-loves-postgresql/)
7. Pour la partie SQL, pensez à utiliser `generated always as identity`
   ([https://www.postgresql.org/docs/current/sql-createtable.html](https://www.postgresql.org/docs/current/sql-createtable.html)) et `generate_series`
   ([https://www.postgresql.org/docs/current/functions-srf.html](https://www.postgresql.org/docs/current/functions-srf.html)).
8. Mon mari prof me dit que les élèves ne lisent jamais les sujets jusqu'au
   bout, c'est vrai ?    

