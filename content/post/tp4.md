---
title: "TP4: SQL"
date: 2021-10-21T14:40:39+02:00
---

## But de ce TP
Le but de ce TP est d'entrevoir les possibilités du SQL moderne. Nous allons
donc regarder du SQL avancé comme les jointures latérales, les CTE (Common Table
Expression), les aggrégations avancées et les CTE récursives.

## Travail demandé
Il vous est demandé durant ce TP d'écrire un rapport comportant les commandes
que vous avez lancées.

Le SQL devra être écrit de manière soignée (indentations, retour à la ligne,
commentaires, sauts de lignes). Il est rappelé que depuis l'invention de la
coloration syntaxique, il n'est plus nécessaire de mettre les mots clés SQL en
majuscules.
Nous vous conseillons d'utiliser des CTE plutôt que des sous-requêtes, cela
améliore la lisbilité et donc la maintenabilité du code.

## Import de données
Les données sont accessibles ici:
[https://l_avrot.gitlab.io/tiw-postgres/chinook.sql.gz](https://l_avrot.gitlab.io/tiw-postgres/chinook.sql.gz)

Il s'agit d'un fichier dump généré avec la commande `pg_dump -Fc chinook | gzip >  chinook.sql.gz`. Commencer par créer une base de données nommée chinook et 
vous devriez ensuite pouvoir importer le fichier en faisant `gunzip -c chinook.sql.gz | psql chinook`.

Le modèle de données de chinook correspond à celui d'un service de streaming de
musique en ligne.

## Jointure latérale
Nous allons commencer avec une fonctionnalité très utile: les jointures
latérales. Pour chaque employé (2 colonnes, nom et prénom), vous fournirez les
données de son•a supérieur•e direct•e (nom et prénom aussi, sur 2 colonnes).

Pour de l'aide sur l'utilisation de lateral, regardez ici:
[https://www.postgresql.org/docs/current/queries-table-expressions.html#QUERIES-FROM](https://www.postgresql.org/docs/current/queries-table-expressions.html#QUERIES-FROM)

## Aggrégations avancées
### Grouping sets
Vous connaissez probablement les fonctions d'aggrégation simple (count, sum,
avg...) avec ou sans clause `having` pour filtrer sur les aggrégats, mais nous
allons aller encore plus loin en utilisant les grouping sets.

Nous allons dans un premier temps calculer le total de chaque facture (un simple
`group by` devrait suffire). Puis nous calculerons le total de toutes les
factures pour chaque mois de l'année 2012 et la dernière ligne donnera le total
pour toute l'année 2012 (la colonne mois pour cette ligne pourra être nulle).

Pour de l'aide sur les grouping sets, regardez ici:
[https://www.postgresql.org/docs/current/queries-table-expressions.html#QUERIES-GROUPING-SETS](https://www.postgresql.org/docs/current/queries-table-expressions.html#QUERIES-GROUPING-SETS)

### Window functions
Vous souhaitez récupérer en une seule requête pour chaque client son nom, son
prénom ainsi que le nombre total de clients.

Pour de l'aide sur les window functions, regardez ici:
[https://www.postgresql.org/docs/current/tutorial-window.html](https://www.postgresql.org/docs/current/tutorial-window.html)

## Recursive CTE
Trouver tous les employés qui sont sous les ordres de "Andrew" (FirstName)
"Adams" (LastName) directement ou indirectement.

Pour de l'aide sur les CTE recursives, regardez ici:
[https://www.postgresql.org/docs/current/queries-with.html#QUERIES-WITH-RECURSIVE](https://www.postgresql.org/docs/current/queries-with.html#QUERIES-WITH-RECURSIVE)

![Xena très fâchée](/images/xena.jpg)

## Vous êtes perdu?

1. Le SQL s'écrit petit à petit avec des essais et des erreurs. C'est normal.
   Commencez par écrire une petite partie de ce que vous voulez.
2. Le site [https://www.pgexercises.com/](https://www.pgexercises.com/) peut
   vous aider à pratiquer le SQL avancé
3. Postgres ne trouve pas votre colonne? Tous les identifiants non entourés de
   double quotes sont case insensitive en SQL. Postgres les traduit en
   minuscules. Si vous avez des majuscules dans vos identifiants, il faudra
   toujours les entourer de doubles quotes et utiliser la bonne casse (ce qui
   n'est pas pratique).
3. Je suis là pour vous aider.

