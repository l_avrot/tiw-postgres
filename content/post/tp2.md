---
title: "TP2: Restauration de données"
date: 2020-10-22T11:47:31+02:00
tag: "tp2"
---

## But de ce TP

Le but de ce TP est d'arriver à restaurer un cluster PostgreSQL en utilisant
différentes sauvegardes. L'outil de sauvegarde préconisé est `pgBackRest`.
Même si cet outil ne fait pas partie du projet PostgreSQL, c'est un des
meilleurs outils de sauvegarde physique utilisable aujourd'hui.

Vous êtes libre d'utiliser un autre outil si vous le souhaitez, mais il devra
forcément vous permettre de faire du Point In Time Recovery (PITR).

## Travail demandé
Nous vous demandons de fournir en fin de TP un "runbook" de vos manipulations.

>Dans un système informatique ou un réseau, un runbook, retranscrit
>littéralement dossier d'exploitation, est une compilation systématique des
>procédures et des opérations que l'administrateur ou l'opérateur du système
>effectue.

Voir [Wikipedia](https://fr.wikipedia.org/wiki/Runbook)

Voici un exemple de runbook: [How to deploy WordPress with highly available
PostgreSQL](https://www.enterprisedb.com/postgres-tutorials/how-deploy-wordpress-highly-available-postgresql)

Au fil de votre runbook, vous donnerez les réponses aux questions suivantes:

- Qu'est-ce qu'une sauvegarde full ?
- Qu'est-ce qu'une sauvegarde incrémentale ?
- Quelle est la différence entre une sauvegarde incrémentale et une sauvegarde
différentielle ?
- Que signifie PITR ?
- Quelle est la différence entre une sauvegarde physique et une sauvegarde
logique ?
- Que signifie WAL ?
- Pourquoi a-t-on besoin des WALs lors d'une sauvegarde physique ?

## Des données
Restaurer une base de données vide n'a que peu d'intérêt. Nous allons utiliser
l'outil `pgbench` fourni avec PostgreSQL pour générer des données dans une
base `benchdb` que vous aurez créée auparavant.

L'initialisation des tables de l'outil `pgbench` se fera avec un scale factor de 10.

Vous fournirez un comptage des lignes des tables `pgbench_tellers` et
`pgbench_accounts`.

## Restauration
L'outil officiel pour effectuer des sauvegardes d'un cluster Postgres est
`pg_basebackup`, malheureusement, cet outil est limité par son design puisqu'il
a été pensé pour la sauvegarde et non pour la restauration des données.

Je vous propose donc d'utiliser `pg_backrest` qui est beaucoup plus convivial.

Suivez ensuite les étapes suivantes :

1. Effectuez une sauvegarde full.
2. Supprimez la totalité des lignes de la table `pgbench_tellers`.
3. Effectuez une sauvegarde incrémentale.
4. Supprimez les lignes de `pgbench_accounts` pour lesquelles la colonne `bid`
vaut 2.
5. Vérifiez le nombre de lignes dans la table `pgbench_accounts`.
6. Restaurez la base dans l'état dans lequel elle était avant l'étape 4.
7. Vérifiez le nombre de lignes dans la table `pgbench_accounts`.
8. Restaurez la base dans l'état dans lequel elle était avant l'étape 2.
9. Vérifiez le nombre de lignes dans les tables `pgbench_tellers` et
 `pgbench_accounts`.

## Retrouver une erreur dans des WALs
La plupart du temps, nous n'avons qu'une idée approximative de l'heure où une
requête destructrice a été envoyée en production. Nous allons "ouvrir" les WALs
pour voir ce qu'il y a dedans.

Pour corser un peu les choses, assurez vous de simuler des transactions en
utilisant `pgbench` ainsi:

```
while :; do pgbench -c 4 -j 1 -T 60 benchdb; sleep 1; done
```

Ensuite, faites "la boulette" en supprimant les lignes de `pgbench_accounts`
pour lesquelles la colonne `bid` vaut 2.

Avant d'aller regarder les WALs, vous aurez besoin de pouvoir identifier la
table avec son oid ainsi que le tablespace et la base de données. Voici la
requête permettant de récupérer ces données:

```
select
coalesce(tbs.oid, db.dattablespace) as tablespace,
db.oid as database,
t.relfilenode as table
from pg_class t left outer join pg_tablespace tbs
on t.reltablespace=tbs.oid
cross join pg_database db
where t.relname='pgbench_accounts'
and db.datname=current_database();
```

L'outil à utiliser pour regarder dans les WALs est `pg_waldump` (voir la
 [documentation](https://www.postgresql.org/docs/current/pgwaldump.html)).

Il ne reste plus qu'à comprendre la sortie de `pg_waldump` pour pouvoir isoler
la requête qui a détruit des données pour trouver le point dans le temps auquel
vous pouvez restaurer vos données en en perdant le moins possible.

## Vous êtes perdu ?
Si vous ne savez pas par quoi commencer, voici des questions pour vous aiguiller:

1. Quelle est la manière corecte d'installer un nouvel outil en production ?
2. Comment peut-on rechercher un package avec Ubuntu ?
3. Pour faire une sauvegarde, recherchez la documentation de votre outil de
   sauvegarde.
4. Pour faire une restauration, la documentation devrait également vous aider.
5. Si vous constatez un comportement surprenant, lisez le log de Postgres (voir
   [What's wrong with Postgres
   ?](https://mydbanotebook.org/post/troubleshooting-02/) ).
6. Si vous ne comprenez toujours pas, demandez-vous quel backup set est pris par
   défaut par `pgBackRest`.
6. Si vous ne savez pas où trouver vos fichiers Wals archivés, c'est peut-être
   que vous avez raté une étape de configuration de votre sauvegarde.
7. Je suis là pour vous aider, n'hésitez pas à poser des questions.
