---
title: "TP5: Optimisation"
date: 2022-01-18T12:40:39+02:00
---

## But de ce TP
L'objectif de ce TP est d'éfleurer ce qu'est l'optimisation. L'optimisation de
requêtes SQL est un constant équilibre à trouver entre l'amélioration des
performances d'une requête particulière sans dégrader les performances de toutes
les autres requêtes.

Les données sont accessibles ici:
[https://mega.nz/folder/xJYA1TAZ#6DSD1plD02crT9-kiySj_g](https://mega.nz/folder/xJYA1TAZ#6DSD1plD02crT9-kiySj_g).
Dans ce répertoire, vous trouverez:

- des fichiers sql pour créer la structure de vos tables
- des fichiers de données compressés
- une archive tar comportant tout le répertoire si vous souhaitez tout
    télécharger d'un coup

## Travail demandé
Il vous est demandé durant ce TP d'écrire un rapport comportant les commandes
que vous avez lancées.

Tout au long de ce rapport vous répondrez aux questions suivantes:

- Pourquoi faut-il toujours lancer une commance `vacuum analyze` après un import
    de données ?
- Comment peut-on analyser les plans d'exécutions avec PostgreSQL ?
- Que signifient les différentes données fournies dans le plan d'exécution ?
- Que pouvez-vous dire sur l'impact des index sur les requêtes d'écriture
    ? Comment l'expliquez-vous ?

## Import de données
La base de données comporte deux tables, objects et sources. Il  vous est
demandé d'importer toutes les données fournies, même si cette opération est
lente.

Vous pouvez utiliser une boucle bash pour importer les données.

Importez toutes les données pour pouvoir faire le TP correctement.

## Analyse de requête en lecture
Vous commencerez par écrire les trois requêtes suivantes:

- Pour chaque objet de la table objects, afficher le nombre de sources de la
    relation sources qui correspond à cet objet.
- Afficher les paires de sources qui ont la même valeur de RA arrondi à 0.1
    près.
- Donner les objets qui ne sont pas associés à une source.

Vous analyserez et améliorerez les temps de réponse dei ces trois requêtes.

## Analyse de requête en écriture
Pour comprendre l'impact de tous les index créés précédement sur une requête
d'écriture, nous comparerons les performances de la requête suivante, avec et
sans index.

La requête SQL devra mettre à jour l'attribut ra de la table source avec la
valeur 2*ra².

## Vous êtes perdu ?

Si vous ne savez pas par quoi commencer, voici des questions et notes pour vous
aiguiller:

1. Rappelez-vous que la meilleure manière d'optimiser une requête SQL, c'est de
   supprimer tout ce qui est inutile.
2. [use-the-index-luke.com](https://use-the-index-luke.com/) peut vous aider.
3. Je suis là pour vous aider ;-)

