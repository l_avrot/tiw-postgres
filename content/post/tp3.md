---
title: "TP3: Réplication"
date: 2020-10-23T17:42:48+02:00
tag: "tp3"
---

## But de ce TP
Le but de ce TP est de mettre en place une réplication physique d'un de vos
cluster Postgres. Pour ce faire, vous allez devoir supprimer le cluster présent
sur l'une de vos VM (pour faire de la place, vos VMs somt limitées en
ressources). Nous vous conseillons de garder le cluster qui comprend votre base
de données `benchdb` créée au TP2.

Une réplication physique peut-être utilisée soit pour augmenter la haute
disponbilité (diminution des RTO et RPO) soit pour permettre une répartition de
charge des requêtes de lecture. Dans ce dernier cas, une réplication synchrone
sera plus appropriée.

## Un peu de vocabulaire
Les termes "maître" et "esclave" ont été supprimés du vocabulaire de la
communauté de PostgreSQL pour désigner les différents nœuds d'une réplication.
Ces termes rappelle des périodes peu glorieuses pour l'humanité et sont surtout
de mauvaises analogies tant l'histoire a prouvé qu'à la mort d'un maître, c'est
rarement un esclave qu'on utilise pour prendre sa suite.

Je vous propose d'utiliser une terminologie basée sur le monde des abeilles:
Reine/Princesse/Ouvrière. Je vous laisse lire [l'excellent
article](https://tapoueh.org/blog/2017/12/queen-princesses-and-workers/) de 
[Dimitri](https://tapoueh.org/) sur le sujet.

## Travail demandé
Nous vous demandons de fournir en fin de TP un "runbook" de vos manipulations.

>Dans un système informatique ou un réseau, un runbook, retranscrit
>littéralement dossier d'exploitation, est une compilation systématique des
>procédures et des opérations que l'administrateur ou l'opérateur du système
>effectue.

Voir [Wikipedia](https://fr.wikipedia.org/wiki/Runbook)

Voici un exemple de runbook: [How to deploy WordPress with highly available
PostgreSQL](https://www.enterprisedb.com/postgres-tutorials/how-deploy-wordpress-highly-available-postgresql)

Au fil de votre runbook, vous donnerez les réponses aux questions suivantes:

- Quelle est la différence entre une réplication logique et une réplication
    physique ?
- Qu'est-ce qu'un réplication en WAL shipping ?
- Qu'est-ce qu'une réplication en mode "streaming" ?
- Une réplication synchrone peut-elle fonctionner en WAL shipping ?
- Quels sont les inconvénients d'une réplication synchrone ?

## La réplication
Vous mettrez en place une réplication streaming en expliquant les différentes
étapes à suivre dans votre runbook.

Vous pouvez continuer à utiliser l'outil de sauvegarde que vous avez choisi lors
du TP2.

Pour vous simplifier la vie, regardez attentivement la documentation de votre
outil de sauvegarde. Il y a peut-être des options utiles pour mettre en place
une réplication.

Enfin, vous modifierez la réplication pour qu'elle devienne synchrone.

## Vérification
Pour vous assurer que votre réplication fonctionne correctement, vous essayerez
chacune des techniques suivantes:

- Regarder l'état de la réplication dans une [vue
    système](https://www.postgresql.org/docs/current/monitoring-stats.html#MONITORING-PG-STAT-REPLICATION-VIEW) 
- Modifier des données sur la reine et vérifier la modification de ces données
    sur la princesse
- Changer de fichier WAL courant et vérifier le fichier WAL courant sur la
    princesses (voir [la documentation sur les fonctions
    d'administration](https://www.postgresql.org/docs/current/functions-admin.html#FUNCTIONS-ADMIN-BACKUP))
- Pour la réplication synchrone, arrêter la princesse et essayer d'écrire sur la reine

## Vous êtes perdu ?
Si vous ne savez pas par quoi commencer, voici des questions et notes pour vous
aiguiller:

1. De quoi a-t-on besoin pour mettre en place une réplication physique ?
2. Comment fonctionne la réplication physique ?
3. Je vous recommande de regarder
   [cette superbe video](https://www.youtube.com/watch?v=GobQw9LMEaw) de [Josh Berkus](https://twitter.com/fuzzychef)
4. [Ça vous semble trop simple ?](https://www.youtube.com/embed/fAWJdXJvngU)
